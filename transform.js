const fs = require('fs')

const comic = JSON.parse(fs.readFileSync('comic.json')).find(c => !!c.comic_image_url);
if (!comic) {
  throw new Error('Could not find comic for today!');
}
const title = comic.title.rendered
const { mouseover } = comic.acf || {}
const comicUrl = comic.link
const imageUrl = comic.comic_image_url

const payload = {
  text: mouseover
    ? `${title}\n\n${mouseover}\n\n${comicUrl}\n`
    : `${title}\n\n${comicUrl}\n`,
  mediaUrls: [imageUrl]
}
fs.writeFileSync('payload.json', JSON.stringify(payload))
