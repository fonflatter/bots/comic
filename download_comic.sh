#!/usr/bin/env bash

set -o errexit
set -o nounset

COMIC_DATE=$1

URL="https://fonflatter.de/wp-json/wp/v2/posts/?after=${COMIC_DATE}T00:00:00&before=${COMIC_DATE}T23:00:00&per_page=1"
echo "Downloading from ${URL}…"
curl "${URL}" > comic.json
